# Performance side channel: cooperative version

## Walkthrough 

The sender keeps flushing memory, and the receiver may judge whether it lies on the same machine with the sender by calculating the number of ticks. To achieve the best difference, launch several sender at the same time.

To manually run the test, run the receiver until it outputs 'a' and prompts an input. Then launch several senders (2 as the team used) and wait until a message saying 'flush finished' is shown. Continue the receiver with an enter and it will output the result at the last. 
 
## gvisor runner

```
make -f Makefile.Docker
python run.gvisor.py
```

