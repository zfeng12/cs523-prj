import subprocess
import time
import sys
import os

def call(sender=False):
    rcmd = ["docker", "container", "run", "--rm", "--runtime=runsc", "--interactive", "--security-opt", "apparmor=unconfined", "receiver"]
    p = subprocess.Popen(rcmd,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        shell=False)
    ps = []
    while True:
        output = p.stdout.readline()
        if len(output.strip()) == 0:
            print >> sys.stderr, "finished"
            for pp in ps:
                pp.terminate()
            x = subprocess.check_output(["docker", "container", "ls", "-a", "-q"])
            y = ["docker", "container", "rm", "-f"]
            y.extend(x.split('\n')[:-1])
            subprocess.call(y, stdout=FNULL, stderr=FNULL)
            break
        elif output.strip() == 'a':
            print >> sys.stderr, 'waiting'
            ps = []
            if sender:
                for k in range(2):
                    print >> sys.stderr, 'flushing'
                    
                    scmd = ["docker", "container", "run", "--rm", "--interactive",  "--security-opt", "apparmor=unconfined", "--runtime=runsc", "sender"]
                    ps.append(subprocess.Popen(scmd,
                        stdin=subprocess.PIPE,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT))
                    output2 = ps[-1].stdout.readline()
                    if output2.strip() != 'flush finished':
                        print(output2.strip())
                        raise Exception
                    print >> sys.stderr, output2.strip()
            time.sleep(0.2)
            print >> sys.stderr, "starting"
            p.stdin.write('\n')
        else:
            print(sender, output.strip())
        
FNULL = open(os.devnull, 'w')
for i in range(5):        
    print >> sys.stderr, "================= EPOCH {0}".format(i)
    call(True)
    call()
