#include <iostream>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

inline void clflush(volatile void *p) {
    asm volatile ("clflush (%0)" :: "r"(p));
}

inline uint64_t rdtsc() {
    unsigned long a, d;
    asm volatile ("cpuid; rdtsc" : "=a" (a), "=d" (d) : : "ebx", "ecx");
    return a | ((uint64_t)d << 32);
}

int main() {
	long long size = 36700160 * 50;
	long long N = 1;
	volatile char *buffer = (char*) malloc(size * sizeof(char));
	for (long long it = 0; it < size; it++)
		buffer[it] = 'x';
	cout << "a" << endl;
	getchar();
	for (int it = 0; it < N; it++) {
		uint64_t start, end, clock = 0;
                for (int it = 0; it < size; it += 64) {
			if (it%50000==0) cout << (double)it/size*100 <<'%' << endl;
			start = rdtsc();
       			volatile char tempC = buffer[it];
               		end = rdtsc();
			clock += (end - start);
		}
		cout << "took " << clock << " ticks\n";
	}
}
