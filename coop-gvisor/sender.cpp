#include <iostream>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

//#define TEST_FLUSH
using namespace std;

inline void clflush(volatile void *p) {
    asm volatile ("clflush (%0)" :: "r"(p));
}

inline uint64_t rdtsc() {
    unsigned long a, d;
    asm volatile ("cpuid; rdtsc" : "=a" (a), "=d" (d) : : "ebx", "ecx");
    return a | ((uint64_t)d << 32);
}

int main() {
	long long size = 36700160 * 50;
	long long N = 10;
	char *buffer = (char*) malloc(size * sizeof(char));
	memset(buffer, 'y', size);
	uint64_t clock;

#ifdef TEST_FLUSH
	cout << "Test no flush" << endl;
	clock = 0;
	for (int it = 0; it < N; it++) {
		uint64_t start, end;
		start = rdtsc();
		volatile char tempC = buffer[135];
		end = rdtsc();
		clock += (end - start);
	}
	cout << "took " << clock << " ticks\n";

	cout << "Test with flush" << endl;
	clock = 0;
	for (int it = 0; it < size; it += 64)
		clflush((unsigned char*) buffer + it);

	for (int it = 0; it < N; it++) {
		uint64_t start, end;
		start = rdtsc();
		volatile char tempC = buffer[135];
		end = rdtsc();
		clock += (end - start);
		for (int it = 0; it < size; it += 64)
                	clflush((unsigned char*) buffer + it);
	}
	cout << "took " << clock << " ticks\n";
#else
	for (int it = 0; it < size; it += 64)
        	clflush((unsigned char*) buffer + it);
	cout << "flush finished" << endl;
	while(true);
#endif
}
