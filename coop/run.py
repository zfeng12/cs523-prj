import subprocess
import time
import sys

def call(sender=False):
    rcmd = ["./receiver"]
    p = subprocess.Popen(rcmd,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT)
    ps = []
    while True:
        output = p.stdout.readline()
        if len(output.strip()) == 0:
            print >> sys.stderr, "finished"
            for pp in ps:
                pp.terminate()
            break
        elif output.strip() == 'a':
            print >> sys.stderr, 'waiting'
            ps = []
            if sender:
                for k in range(2):
                    print >> sys.stderr, 'flushing'
                    
                    scmd = ["./sender"]
                    ps.append(subprocess.Popen(scmd,
                        stdin=subprocess.PIPE,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT))
                    output2 = ps[-1].stdout.readline()
                    if output2.strip() != 'flush finished':
                        print(output2.strip())
                        raise Exception
                    print >> sys.stderr, output2.strip()
            time.sleep(0.2)
            print >> sys.stderr, "starting"
            p.stdin.write('\n')
        else:
            print(sender, output.strip())
        
for i in range(10):        
    print >> sys.stderr, "================= EPOCH {0}".format(i)
    call(True)
    call()
#out, err d= p.communicate()
#print(out)
#for line in iter(p.stdout.readline, b''):
#    print(">>> "+line.rstrip())
