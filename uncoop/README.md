# Performance side channel: cooperative version

## Walkthrough 

The sender keeps flushing memory, and one client service keeps posting requests to the server, measuring the respond time. The client may judge whether it lies on the same machine with the sender by calculating the number of ticks. To achieve the best difference, launch several sender at the same time.

## Procedure



First start a Redis server, compile the programs properly using `make -f Makefile.Docker` and run `python init.go`. In this experiment, we choose to test the `get` queries so initialization is a must.

Then place `get.go` in a remote server. First deploy the commands properly and run without the company of sender. run the sender several times (2 in our experiments) and then `get.go`, or use `run.docker.py` to automate the process.

