package main

import (
	"log"
	"fmt"
	"os/exec"
	"sync"
	"math/rand"
	"strconv"
	"time"
)

var t time.Duration
var m sync.Mutex

func f(wg *sync.WaitGroup){
	defer wg.Done()
	cmd := "echo get "+ strconv.Itoa(rand.Intn(1000000)) + " | redis-cli -h fa19-cs523-24.cs.illinois.edu"
	l := time.Now()
	_, err := exec.Command("bash", "-c", cmd).Output()
	x := time.Since(l)
	m.Lock()
	t = t+x
	m.Unlock()
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Printf("%s", out)
}


func main(){
	t = 0
	var wg sync.WaitGroup
	for j := 0; j < 10; j++ {
		for i := 0; i < 200; i++ {
			wg.Add(1)
			go f(&wg)
		}
		wg.Wait()
	}
	fmt.Println(t)

}
