import subprocess
import time
import sys
import os

def call():
    FNULL = open(os.devnull, 'w')
    
    print >>sys.stderr, "SENDER STARTS"
    scmd = ["docker", "container", "run", "--rm", "--interactive", "--security-opt", "apparmor=unconfined", "sender"]
    p = subprocess.Popen(scmd,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        shell=False)
    
    time.sleep(6)
    print >>sys.stderr, "CLIENT STARTS WITH SENDER"
    
    for i in range(1):
        t = subprocess.check_output(["ssh", "zfeng12@fa19-cs523-23.cs.illinois.edu", "./test/get"])
        print "+sender", i, t[:-1]

    print >>sys.stderr, "CLIENT FINISHES"
    x = subprocess.check_output(["docker", "container", "ls", "-f", "ancestor=sender", "-a", "-q"])
    y = ["docker", "container", "rm", "-f"]
    y.extend(x.split('\n')[:-1])
    subprocess.call(y, stdout=FNULL, stderr=FNULL)

    print >>sys.stderr, "SENDER KILLED"

    time.sleep(6)
    print >>sys.stderr, "CLIENT STARTS WITHOUT SENDER"

    for i in range(3):
        t = subprocess.check_output(["ssh", "zfeng12@fa19-cs523-23.cs.illinois.edu", "./test/get"])
        print "-sender", i, t[:-1]

    

for i in range(10):
    print >>sys.stderr, "========== EPOCH ", i
    call()
